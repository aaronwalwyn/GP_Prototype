﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public static Room[,] roomsArray = new Room[11, 11];
	public static int roomWidth = 16, roomHeight = 9;
	public static Vector2 bottomLeft;

	// Use this for initialization
	void Start () {
		RoomGeneration ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	int maxRooms = 60, roomsCount;
	Queue<Room> openSet = new Queue<Room> ();

	GameObject roomParent;
	Room currentRoom;
	void RoomGeneration() {
		bottomLeft = Vector2.zero - Vector2.right * (roomWidth * roomsArray.GetLength (0)) / 2f - Vector2.up * (roomHeight * roomsArray.GetLength (1)) / 2f;
		Debug.Log (bottomLeft.ToString ());

		roomsCount = 0;

		roomParent = new GameObject ("Rooms");

		GenerateRoom (5, 5);

		while (openSet.Count > 0 && roomsCount < maxRooms) {
			currentRoom = openSet.Dequeue ();

			if(Random.value < 0.5f && currentRoom.indexX+1 < roomsArray.GetLength(0) && roomsArray[currentRoom.indexX +1, currentRoom.indexY] == null) {
				GenerateRoom (currentRoom.indexX+1, currentRoom.indexY);
			}

			if(Random.value < 0.5f && currentRoom.indexX-1 > -1 && roomsArray[currentRoom.indexX-1, currentRoom.indexY] == null) {
				GenerateRoom (currentRoom.indexX-1, currentRoom.indexY);
			}

			if(Random.value < 0.5f && currentRoom.indexY+1 < roomsArray.GetLength(1)&& roomsArray[currentRoom.indexX, currentRoom.indexY+1] == null) {
				GenerateRoom (currentRoom.indexX, currentRoom.indexY+1);
			}

			if(Random.value < 0.5f && currentRoom.indexY-1 > -1&& roomsArray[currentRoom.indexX, currentRoom.indexY-1] == null) {
				GenerateRoom (currentRoom.indexX, currentRoom.indexY-1);
			}
		}

		for (int x = 0; x < roomsArray.GetLength (0); x++) {
			for (int y = 0; y < roomsArray.GetLength (1); y++) {
				Room r = roomsArray [x, y];
				if (r != null) {
					GameObject g = new GameObject ("Floor");
					g.transform.parent = r.transform;
					SpriteRenderer sr = g.AddComponent<SpriteRenderer> ();
					Sprite sp = sr.sprite = Sprite.Create (new Texture2D (roomWidth, roomHeight), new Rect(Vector2.zero, new Vector2( roomWidth, roomHeight)), Vector2.one * 0.5f);
					sr.color = Random.ColorHSV ();
					g.transform.localScale = new Vector3 (100f, 100f);
					//sp.bounds.size = new Vector2 (LevelManager.roomWidth, LevelManager.roomHeight);
				}
			}
		}
	}

	void GenerateRoom(int _x, int _y) {
		GameObject room = new GameObject ("room");
		roomsArray[_x,_y] = room.AddComponent<Room> ();
		roomsArray[_x,_y].SetIndex (_x, _y);
		room.transform.parent = roomParent.transform;

		openSet.Enqueue(roomsArray[_x,_y]);
		roomsCount++;
	}
}
